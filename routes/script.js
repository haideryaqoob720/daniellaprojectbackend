const request = require('request');
const cheerio = require('cheerio');

module.exports = app => {

    app.get('/tokenHolders', (req, res) => {



        var chain;

        const ethApiKey = 'GNR3S6CRQCVT498XDXQZBTS6UA3FSC5SY9';
        const bscApiKey = 'YDC4AYR8M6F4PQ9B7KNF68KE5J95UMBY3Y'

        if (req.query.tokenChain === "ETH"){
                const options1 = {
                    method: 'GET',
                    url: `https://etherscan.io/token/${req.query.tokenAddress}`,
                    headers: { 'content-type': 'application/json' },
                    json: true
                };
                request(options1, (error, response, html) => {
                    if (!error && response.statusCode == 200) {
                        const $ = cheerio.load(html);
    
                        const holders = $('div[id = "ContentPlaceHolder1_tr_tokenHolders"] > div > div').text().trim();
    
                        console.log(holders);
                        res.send({ chain, holders });
                    } else if (error) throw new Error(error);
                });
            }else if (req.query.tokenChain === "BSC"){
                const options1 = {
                    method: 'GET',
                    url: `https://bscscan.com/token/${req.query.tokenAddress}`,
                    headers: { 'content-type': 'application/json' },
                    json: true
                };
                request(options1, (error, response, html) => {
                    if (!error && response.statusCode == 200) {
                        const $ = cheerio.load(html);
    
                        const holders = $('div[id = "ContentPlaceHolder1_tr_tokenHolders"] > div > div').text().trim();
    
                        console.log(holders);
                        res.send({ chain, holders });
                    } else if (error) throw new Error(error);
                });
            }

    });

    app.get('/tokenDetails', (req, res) => {

        var chain;

        const ethApiKey = 'GNR3S6CRQCVT498XDXQZBTS6UA3FSC5SY9';
        const bscApiKey = 'YDC4AYR8M6F4PQ9B7KNF68KE5J95UMBY3Y'

        const options = {
            method: 'GET',
            url: `https://api.etherscan.io/api?module=contract&action=getabi&address=${req.query.tokenAddress}&apikey=${ethApiKey}`,
            headers: { 'content-type': 'application/json' },
            json: true
        };
        request(options, function (error, response, body) {

            if (error) throw new Error(error);
            else if (response.body.status == 1) {
                chain = "ETH"
                console.log(`response of first request, ${response.body.status} and chain is, ${chain}`)

                const options1 = {
                    method: 'GET',
                    url: `https://etherscan.io/token/${req.query.tokenAddress}`,
                    headers: { 'content-type': 'application/json' },
                    json: true
                };
                request(options1, (error, response, html) => {
                    if (!error && response.statusCode == 200) {
                        const $ = cheerio.load(html);
    
                        const holders = $('div[id = "ContentPlaceHolder1_tr_tokenHolders"] > div > div').text().trim();
    
                        console.log(holders);
                        res.send({ chain, holders });
                    } else if (error) throw new Error(error);
                });

            }
            else if (response.body.result === "Invalid Address format") {
                res.send("Invalid Address format");
            }
            else if (response.body.status == 0) {
                chainStatus = 0;
                const options = {
                    method: 'GET',
                    url: `https://api.bscscan.com/api?module=contract&action=getabi&address=${req.query.tokenAddress}&apikey=${bscApiKey}`,
                    headers: { 'content-type': 'application/json' },
                    json: true
                };
                request(options, function (error, response, body) {
                    if (error) throw new Error(error);

                    else if (response.body.status == 1) {
                        chain = "BSC"
                        console.log(`response of second request, ${response.body.status} and chain is, ${chain}`)

                        const options1 = {
                            method: 'GET',
                            url: `https://bscscan.com/token/${req.query.tokenAddress}`,
                            headers: { 'content-type': 'application/json' },
                            json: true
                        };
                        request(options1, (error, response, html) => {
                            if (!error && response.statusCode == 200) {
                                const $ = cheerio.load(html);
            
                                const holders = $('div[id = "ContentPlaceHolder1_tr_tokenHolders"] > div > div').text().trim();
            
                                console.log(holders);
                                res.send({ chain, holders });
                            } else if (error) throw new Error(error);
                        });

                    }
                    else if (response.body.result === "Invalid Address format") {
                        res.send("Invalid Address format");
                    }
                    else if (response.body.status == 0) {
                        console.log("token not exist on eth and bsc")
                        res.send(chain, "token not exist on eth and bsc")
                    }

                });
            }
        });

    });
}



